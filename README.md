# ics-ans-role-netplan

Ansible role to install and configure netplan on Ubuntu.

## Role Variables

```yaml
netplan_all_vlans_on_bond: false  # define is the VLANS should be added a tagged interfaces to the bond from csentry inventory

netplan_pri_domain: # define DNS search 
netplan_config_file:   #which file should be used to store netpaln config

netplan_autoinstall_file: # wich config file to disabled (rename) after autoinstall
netplan_main_if_network:  # define wich interface will be used to define default GW by specifying the network name

netplan_static_route_for_network:  # define wich network needs static for Management/ansible by specifying the network name of the interface
netplan_static_routes:   # list of static route to add the the Mgmt interface 
  - to: 10.0.10.0/24
  via: 10.0.1.254
  - to: 10.0.20.0/24
  via: 10.0.1.254


# net plan config
netplan_configuration:
  network:
    version: 2
    ethernets:
      enp0s3:
        dhcp4: true
    bonds:
      bond0:
        dhcp4: false
        interfaces:
          - enp0s8
          - enp0s9
        parameters:
          mode: 802.3ad



```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-netplan
```

## License

BSD 2-clause
